package tk.yoshirealms.clans;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public final class Clans extends JavaPlugin {

    public static Clans plugin;
    public static List<Clan> clans = new ArrayList<>();
    public static File clansDataDir = new File(plugin.getDataFolder().getPath() + File.separator + "clans");

    @Override
    public void onEnable() {
        plugin = this;
        loadClans();

    }

    @Override
    public void onDisable() {
        saveClans();
    }

    /**
     * Load all the clans currently stored in the config directory.
     */
    public static void loadClans() {

        if (clansDataDir.listFiles() != null) {
            for (File clanFile : clansDataDir.listFiles()) {
                loadClanFromFile(clanFile);
            }
        }
    }

    /**
     * save all clans that have been created.
     */
    public static void saveClans() {
        for (Clan clan : clans) {
            saveClanToFile(clan);
        }
    }

    /**
     * Load a clan from a configuration file
     * @param clanFile The file that contains the clan info.
     */
    public static void loadClanFromFile(File clanFile) {
        YamlConfiguration configuration = YamlConfiguration.loadConfiguration(clanFile);
        HashMap<UUID, Role> members = new HashMap<>();
        for (int i=0;i<configuration.getList("members.UUID").size();i++) {
            members.put(UUID.fromString(configuration.getList("members.UUID").get(i).toString()), Role.valueOf(configuration.getList("members.role").get(i).toString()));
        }
        clans.add(new Clan(configuration.getString("name"), configuration.getString("description"), members, configuration.getInt("money")));
    }

    /**
     * Save a clan to the configuration file.
     * @param clan The clan object that should be stored.
     */
    public static void saveClanToFile(Clan clan) {
        YamlConfiguration config = YamlConfiguration.loadConfiguration(new File(clansDataDir, clan.getName()));
        config.set("name", clan.getName());
        config.set("description", clan.getDescription());
        config.set("members.UUID", clan.getMembers());
        config.set("members.role", clan.getRoles());
        try {
            config.save(new File(clansDataDir, clan.getName()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
