package tk.yoshirealms.clans;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Clan {

    private String name;
    private String description;
    private HashMap<UUID, Role> members = new HashMap<>();
    private int money;

    /**
     * Create a Clan.
     * @param name The name of the Clan.
     * @param owner The Owner of the Clan.
     */
    public Clan(String name, UUID owner) {
        HashMap<UUID, Role> members = new HashMap<>();
        members.put(owner, Role.OWNER);
        new Clan(name, "", members, 0);

    }

    /**
     * Create a clan.
     * @param name The name of the Clan.
     * @param description The description of the Clan.
     * @param members The list of members of the Clan.
     * @param money The amount of money the Clan has.
     */
    public Clan(String name, String description, HashMap<UUID, Role> members, int money) {
        this.name = name;
        this.description = description;
        this.members = members;
        this.money = money;
    }

    /**
     * Get the name of the Clan.
     * @return The name of the Clan.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the Clan.
     * @param name The new name of the Clan.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the description of the Clan.
     * @return The description of the Clan.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the descirption of the Clan.
     * @param description The new description of the Clan.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the a list of members in the Clan.
     * @return A List<UUID> with all the members of the Clan.
     */
    public List<UUID> getMembers() {
        return (List<UUID>) members.keySet();
    }

    /**
     * Add a new member to the Clan.
     * @param uuid The UUID of the new member.
     * @param role The role the new member will have.
     * @return true if success. false if failed.
     */
    public boolean addMember(UUID uuid, Role role) {
        if (members.containsKey(uuid)) {
            return false;
        } else {
            members.put(uuid, role);
            return true;
        }
    }

    /**
     * Remove a member from the Clan.
     * @param uuid The UUID of the player to remove from the Clan.
     * @return true if success. false if failed.
     */
    public boolean removeMember(UUID uuid) {
        if (members.containsKey(uuid)) {
            members.remove(uuid);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Promote a member in the Clan.
     * @param uuid The UUID of the user to promote.
     * @return true if success. false if failed.
     */
    public boolean promoteMember(UUID uuid) {
        if (members.containsKey(uuid)) {
            switch (members.get(uuid)) {
                case MEMBER:  members.put(uuid, Role.MOD);
                case MOD:     members.put(uuid, Role.GENERAL);
                case GENERAL: members.put(uuid, Role.COOWNER);
                case COOWNER: members.put(uuid, Role.OWNER);
                case OWNER:   return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the amount of money that the Clan has.
     * @return The amount of money the Clan has.
     */
    public int getMoney() {
        return money;
    }

    /**
     * Set the money the clan has.
     * @param money The amount of money the Clan should have.
     */
    public void setMoney(int money) {
        this.money = money;
    }

    /**
     * Add a amount of money to the Clans current amount of money.
     * @param money The amount of money that should be added to the total.
     */
    public void addMoney(int money) {
        this.money += money;
    }

    /**
     * Remove a amount of money from the Clans current amount of money.
     * @param money The amount of money that should be removed from the total.
     */
    public void removeMoney(int money) {
        this.money -= money;
    }

    /**
     * Get a List of the roles of all the members.
     * @return A List<Role> of the roles of all members.
     */
    public List<Role> getRoles() {
        return (List<Role>) members.values();
    }
}
